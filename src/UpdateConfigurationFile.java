import java.io.*;
import java.util.Scanner;

/**
 * Created by a on 2015/10/07.
 */
public class UpdateConfigurationFile {

    public static void main(String args[]) throws IOException {
        File file = new File("C:\\Users\\a\\Desktop\\ConfigurationFile.txt");
        file.createNewFile();
        BufferedWriter text = new BufferedWriter(new FileWriter(file));
        text.write("# This is a configuration file in standard configuration file format");
        text.newLine();
        text.write("#");
        text.newLine();
        text.write("# Lines begininning with a hash or a semicolon are ignored by the application");
        text.newLine();
        text.write("# program. Blank lines are also ignored by the application program.");
        text.newLine();
        text.newLine();
        text.write("# The first word on each non comment line is the configuration option.");
        text.newLine();
        text.write("# Remaining words or numbers on the line are configuration parameter");
        text.newLine();
        text.write("# data fields.");
        text.newLine();
        text.newLine();
        text.write("# Note that configuration option names are not case sensitive. However,");
        text.newLine();
        text.write("# configuration parameter data is case sensitive and the lettercase must");
        text.newLine();
        text.write("# be preserved");
        text.newLine();
        text.newLine();
        text.write("# This is a favourite fruit");
        text.newLine();
        text.write("FAVOURITEFRUIT banana");
        text.newLine();
        text.newLine();
        text.write("# This is a boolean that should be set");
        text.newLine();
        text.write("NEEDSPEELING");
        text.newLine();
        text.newLine();
        text.write("# This boolean is commented out");
        text.newLine();
        text.write("; SEEDSREMOVED");
        text.newLine();
        text.newLine();
        text.write("# How many bananas we have");
        text.newLine();
        text.write("NUMBEROFBANANAS 48");
        text.newLine();
        text.newLine();
        text.close();
        try {
            File file1 = new File("C:\\Users\\a\\Desktop\\ConfigurationFile.txt");
            BufferedReader reader = new BufferedReader(new FileReader(file1));
            String oldtxt = "";
            Scanner line = new Scanner(file1);
            while (line.hasNext()) {
                oldtxt += line.next();
                String newtxt = oldtxt.replaceAll("48 ", "1024");
                String newtxt2 = oldtxt.replaceAll("; SEEDSREMOVED ", "SEEDSREMOVED");
                String newtxt3 = oldtxt.replaceAll("NEEDSPEELING", "; NEEDSPEELING");

                BufferedWriter word = new BufferedWriter(new FileWriter(file1, true));
                word.write(newtxt);
                word.write(newtxt2);
                word.write(newtxt3);
                word.close();

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
