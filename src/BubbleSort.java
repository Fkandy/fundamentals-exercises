/**
 * Created by a on 2015/09/21.
 */
public class BubbleSort {
    public static void main(String[] args) {

        int numbers[] = new int[]{52,47,23,74,12,54,38,24,15};
        System.out.println("Array Before Bubble Sort");
        for(int i=0; i < numbers.length; i++){
            System.out.print(numbers[i] + " ");
        }
        bubbleSort(numbers);

        System.out.println("");
        System.out.println("Array After Bubble Sort");
        for(int i=0; i < numbers.length; i++){
            System.out.print(numbers[i] + " ");
        }
    }
    private static void bubbleSort(int[] intArray) {

        int n = intArray.length;
        int swap = 0;

        for(int i=0; i < n; i++){
            for(int j=1; j < (n-i); j++){

                if(intArray[j-1] < intArray[j]){
                    swap = intArray[j-1];
                    intArray[j-1] = intArray[j];
                    intArray[j] = swap;
                }

            }
        }

    }
}
