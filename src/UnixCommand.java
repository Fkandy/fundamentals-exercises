import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by a on 2015/10/01.
 */
public class UnixCommand {
    public static void main(String[] args) throws IOException {
        try {
            String fileName = "sample.txt";
            FileReader fileReader = new FileReader("C:\\Users\\a\\Desktop\\Read.txt");
            BufferedReader in = new BufferedReader(fileReader);
            String line;
            String[] words;
            int totalwords = 0;
            long totalwords1 = 0;
            int lines = 0;
            String chars = " ";
            int countChars = 0;
            while ((line = in.readLine()) != null) {

                lines++;
                if (!line.equalsIgnoreCase("")){
                    countChars++;
                chars += line.length();
                countChars += chars.length();
                }
                words = line.split(" ");
                totalwords += words.length;
                totalwords1 += words(line);

            }
            in.close();
            System.out.println("Total Lines :" + lines);
            System.out.println("Total Characters :" + countChars);
            System.out.println("Total words :" + totalwords1);

        } catch (Exception ex) {

            ex.printStackTrace();

        }

    }

    private static long words(String line) {

        long numWords = 0;
        int index = 0;
        boolean prevWhitespace = true;

        while (index < line.length()) {

            char c = line.charAt(index++);

            boolean currWhitespace = Character.isWhitespace(c);

            if (prevWhitespace && !currWhitespace) {

                numWords++;

            }

            prevWhitespace = currWhitespace;

        }

        return numWords;
    }
}
