import java.util.Scanner;

/**
 * Created by a on 2015/09/21.
 */
public class AddNumbers {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int firstword = Integer.parseInt(scan.next());
        int secondword = Integer.parseInt(scan.next());
        int sum = firstword + secondword;
        System.out.println("Total Sum " + sum);

    }
}
