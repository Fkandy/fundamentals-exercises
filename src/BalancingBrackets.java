/**
 * Created by a on 2015/09/23.
 */
public class BalancingBrackets {

    public static boolean checkBrackets(String str) {
        int unbalancedBrackets = 0;
        for (char ch : str.toCharArray()) {
            if (ch == '[') {
                unbalancedBrackets++;
            } else if (ch == ']') {
                unbalancedBrackets--;
            } else {
                return false;
            }
            if (unbalancedBrackets < 0) {
                return false;
            }
        }
        return unbalancedBrackets == 0;
    }

    static String generateArray(int n) {
        if (n % 2 == 1) {
            return null;
        }
        String str2 = "";
        int openBracketsLeft = n / 2;
        int unclosed = 0;
        while (str2.length() < n) {
            if (((Math.random() >= 5) && (openBracketsLeft > 0)) || (unclosed == 0)) {
                str2 += '[';
                openBracketsLeft--;
                unclosed++;
            } else {
                str2 += ']';
                unclosed--;
            }
        }
        return str2;
    }

    public static void main(String[] args) {
        String ok = "", notOk="";
        String balanced = "";
        String unbalanced = " ";

        String[] tests = {"", "[]", "][", "[][]", "][][", "[[][]]", "[]][[]"};
        for (int i = 0; i <= 16; i += 2) {
            generateArray(i);
        }
            for (String test : tests) {

                if (checkBrackets(test) == true) {
                    ok =  "OK";
                    if (test.isEmpty()) {
                        test =  "(empty):";

                    }
                    balanced = test;
                } else {
                    notOk = "NOT OK";
                    unbalanced = test;
                }
                System.out.printf("%-10s %-10s %-10s %-10s\n", balanced,ok , unbalanced,notOk);
            }
        }


}


