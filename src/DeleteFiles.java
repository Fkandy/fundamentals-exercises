import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by a on 2015/09/21.
 */
public class DeleteFiles {
    public static void main(String[] args) throws IOException {
        File file1 = new File("C:\\Users\\a\\Desktop\\input.txt");

        File file2 = new File("C:\\Users\\a\\Desktop\\docs");

        if (!file2.exists()) {
            file2.mkdir();
            file1.createNewFile();

        } else {
            file2.delete();
            file2.mkdir();
            file1.createNewFile();

        }

        System.out.println("Enter 1 to delete " + file1.getName() + " and " + file2.getName());
        Scanner scan = new Scanner(System.in);
        int delete = Integer.parseInt(scan.next());
        if (delete == 1) {
            file1.delete();
            file2.delete();
            System.out.println("Input.txt and docs files has been deleted");
        } else {
            System.out.println("Wrong input");
        }

    }
}
