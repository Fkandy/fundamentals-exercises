import java.util.Scanner;

/**
 * Created by a on 2015/09/22.
 */
public class EvenorOdd {
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a number : ");
        int number = Integer.parseInt(scan.next());

        if (number % 2== 0) {
            System.out.println("This is an Even");
        } else {
            System.out.println("This is an Odd");
        }
        System.out.println("______________________________");
        System.out.println("Finding number if its even or odd using bitwise AND operator ");
        System.out.print("Enter a number : ");
        int number2 = Integer.parseInt(scan.next());
        if( (number2&1) == 0){
            System.out.println("This is an Even");
        }else{
            System.out.println("This is an Odd");
        }
    }
}
