import java.util.Scanner;

/**
 * Created by a on 2015/09/28.
 */
public class Goldbach {
    public static void main(String[] args)  {
        Scanner src = new Scanner(System.in);
        int userInput;
try{
        System.out.print("Please enter an even integer greater than 2:");
        userInput = Integer.parseInt(src.nextLine());
       if (userInput < 2 || userInput == 2) {
            System.out.print("Please input an integer greater than 2");
            System.exit(0);
        }
        ;
        System.out.println("_______________________________________");
        System.out.println("Goldbach's Conjecture on " + userInput);
        testGoldbach(userInput);
    } catch (NumberFormatException e){
    System.out.println("Please input an integer");
    System.exit(0);
}
    }


    public static boolean IsPrime(int a) {
        boolean check = true;
        for (int i = 2; i < a ; i++) {
            if (a % i == 0)
                check = false;
        }
        return check;
    }


    public static void testGoldbach(int number) {
        int secondnumber = 0;
        for (int firstnumber = 3; firstnumber <= (number / 2); firstnumber = firstnumber + 2) {
            secondnumber = number - firstnumber;
            if (IsPrime(firstnumber) && IsPrime(secondnumber)) {
                System.out.println(number + " = " + firstnumber + " + " + secondnumber);

            }
        }
    }

}

