import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by a on 2015/09/29.
 */
public class PigLatin {
    public static void main(String args[]) throws IOException {
        System.out.print("Enter any word: ");
        Scanner src = new Scanner(System.in);
        String word = src.nextLine();
        int pos = -1;
        char str[] = {'a', 'e', 'i', 'o', 'u'};
        char ch;
        while (!word.equalsIgnoreCase("close")) {
            for (int i = 0; i < word.length(); i++) {
                ch = word.charAt(i);
                if (ch == str[0] || ch == str[1] || ch == str[2] || ch == str[3] || ch == str[4]) {
                    pos = i;
                    break;
                }
            }

            if (word.startsWith("a") || word.startsWith("e") || word.startsWith("i") | word.startsWith("0") || word.startsWith("u")) {
                System.out.println(word + "way");
                System.out.println("Enter any word: ");
                word = src.nextLine();
            } else {
                if (pos != -1) {

                    System.out.println(word.substring(pos) + "-" + word.substring(0, pos) + "ay");
                    System.out.println("Enter any word: ");
                    word = src.nextLine();
                } if(!word.contains("a")||!word.contains("e")||!word.contains("i")|| !word.contains("o")||!word.contains("u") ) {
                    System.out.println("No vowel, hence Pig latin not possible");
                    System.out.println("Enter any word: ");
                    word = src.nextLine();
                }
            }
        }
    }
}