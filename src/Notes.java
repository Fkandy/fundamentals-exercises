import java.io.*;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by a on 2015/10/08.
 */
public class Notes {
    public static void main(String[] args) throws IOException {

        File file = new File("C:\\Users\\a\\Desktop\\NOTES.txt");
        if (!file.exists()) {
            BufferedWriter arg = new BufferedWriter(new FileWriter(file));

            file.createNewFile();
            arg.write("This is a argument");
            arg.newLine();
            arg.close();
        }
        BufferedReader read = new BufferedReader(new FileReader(file));
        String arg = read.readLine();
        System.out.println(arg);
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        long sytemtime = System.currentTimeMillis();
        Date todaydate = new Date(sytemtime);
        String reportDate = df.format(todaydate);
        BufferedWriter writeDate = new BufferedWriter(new FileWriter("C:\\Users\\a\\Desktop\\NOTES.txt", true));
        writeDate.newLine();
        writeDate.write(reportDate);
        writeDate.newLine();
        writeDate.newLine();
        writeDate.close();


    }

}
