import java.io.IOException;
import java.io.StringReader;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * Created by a on 2015/09/28.
 */
public class ReadingXML extends DefaultHandler {
    public static void main(String args[]) throws Exception {

        String xmltext = "<Students>\n" +
                "  <Student Name=\"April\" Gender=\"F\" DateOfBirth=\"1989-01-02\" />\n" +
                "  <Student Name=\"Bob\" Gender=\"M\" DateOfBirth=\"1990-03-04\" />\n" +
                "  <Student Name=\"Chad\" Gender=\"M\" DateOfBirth=\"1991-05-06\" />\n" +
                "  <Student Name=\"Dave\" Gender=\"M\" DateOfBirth=\"1992-07-08\">\n" +
                "    <Pet Type=\"dog\" Name=\"Rover\" />\n" +
                "  </Student>\n" +
                "  <Student DateOfBirth=\"1993-09-10\" Gender=\"F\" Name=\"&#x00C9;mily\" />\n" +
                "</Students>";

        ReadingXML handlerInstance = new ReadingXML();
        handlerInstance.parse(new InputSource(new StringReader(xmltext)));
    }

    public void parse(InputSource src) throws SAXException, IOException {
        XMLReader parser = XMLReaderFactory.createXMLReader();
        parser.setContentHandler(this);
        parser.parse(src);
    }

    public void startElement(String uri, String localName, String Name, Attributes attributes) throws SAXException {
        if (Name.equals("Student")) {
            if (!attributes.getValue("Name").isEmpty()) {
                System.out.println(attributes.getValue("Name"));
            }
        }

    }
}
