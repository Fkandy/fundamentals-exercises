import java.util.Scanner;

/**
 * Created by a on 2015/09/29.
 */
public class RomanNumbers {
     public static void main(String[] args) {
        Scanner src = new Scanner(System.in);

        System.out.println("Enter First Roman Numerals:");
        String firstLowercase = src.nextLine();
         String first = firstLowercase.toUpperCase();

        System.out.println("Enter Second Roman Numerals:");
        String secondLowercase = src.nextLine();
         String second = secondLowercase.toUpperCase();

        int intValue = SumOfRoman(first, second);

        String thou[] = {"", "M", "MM", "MMM"};
        String hund[] = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
        String ten[] = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
        String unit[] = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};


        int th = intValue / 1000;
        int h = (intValue / 100) % 10;
        int t = (intValue / 10) % 10;
        int u = intValue % 10;
        System.out.println("Sum: " + thou[th] + hund[h] + ten[t] + unit[u]);
    }

    public static int SumOfRoman(String first, String second) {
        int n = first.length();
        int m = second.length();

        int sum1 = 0;
        int sum2 = 0;

        int sumOfSameValues1 = 0;
        int sumOfSameValues2 = 0;

        for (int i = 0; i < n; ++i) {
            char currentChar1 = first.charAt(i);
            String strCurrentChar1 = currentChar1 + "";

            char currentChar2 = second.charAt(i);
            String strCurrentChar2 = currentChar2 + "";

            int currentValue1 = valueOf(strCurrentChar1);
            int currentValue2 = valueOf(strCurrentChar2);

            boolean hasNextValue1 = (i < n - 1);
            boolean hasNextValue2 = (i < m - 1);

            sumOfSameValues1 += currentValue1;
            sumOfSameValues2 += currentValue2;

            if (!hasNextValue1 || !hasNextValue2) {
                sum1 += sumOfSameValues1;
                sum2 += sumOfSameValues2;

            } else {
                char nextChar1 = first.charAt(i + 1);
                String strNextChar1 = nextChar1 + "";
                int nextValue1 = valueOf(strNextChar1);
                char nextChar2 = second.charAt(i + 1);
                String strNextChar2 = nextChar2 + "";
                int nextValue2 = valueOf(strNextChar2);
                if (nextValue1 < currentValue1 || nextValue2 < currentValue2) {
                    sum1 += sumOfSameValues1;
                    sumOfSameValues1 = 0;
                    sum2 += sumOfSameValues2;
                    sumOfSameValues2 = 0;
                } else if (nextValue1 > currentValue1 || nextValue2 > currentValue2) {
                    sum1 -= sumOfSameValues1;
                    sumOfSameValues1 = 0;
                    sum2 -= sumOfSameValues2;
                    sumOfSameValues2 = 0;
                }
            }
        }
        int sum = sum1 + sum2;
        return sum;
    }

    static int valueOf(String c) {
        switch (c) {
            case "M":
                return 1000;
            case "D":
                return 500;
            case "C":
                return 100;
            case "L":
                return 50;
            case "X":
                return 10;
            case "IX":
                return 9;
            case "VIII":
                return 8;
            case "VII":
                return 7;
            case "VI":
                return 6;
            case "V":
                return 5;
            case "IV":
                return 4;
            case "III":
                return 3;
            case "II":
                return 2;
            case "I":
                return 1;
            default:
                return 0;
        }
    }
}
