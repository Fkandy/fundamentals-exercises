import java.lang.reflect.Array;

/**
 * Created by a on 2015/09/29.
 */
public class PancakeFlipping {
    public static void main(String args[]) {

        int integers[] = {75, 3, 5, 8, 13, 9, 30, 45, 4, 1};
        System.out.println("Unsorted integers");
        for (int i = 0; i < integers.length; i++) {
            System.out.print(integers[i] + " ");
        }

        sortingIntegers(integers);
        System.out.println("");
        System.out.println("Integer sorted in a ascending order");
        for (int i = 0; i < integers.length; i++) {
            System.out.print(integers[i] + " ");
        }

    }

    private static void sortingIntegers(int[] integers) {
        int loop = 0;
        for (int i = 0; i < integers.length; i++) {
            for (int j = 0; j < integers.length; j++) {
                if (integers[i] < integers[j]) {
                    loop = integers[i];
                    integers[i] = integers[j];
                    integers[j] = loop;

                }
            }
        }
    }
}