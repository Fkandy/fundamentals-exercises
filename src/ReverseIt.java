/**
 * Created by a on 2015/09/21.
 */
public class ReverseIt {
    public static void main(String[] args) {
        String word = "Unapologetic";
        char c[] = word.toCharArray();
        String[] sentence = {"Good ", "things ", "happen ", "to ", "those ", "who ", "wait "};
        System.out.println("The word to reverse is " + word);
        for (int i = c.length - 1; i >= 0; --i) {

            System.out.print(c[i]);
        }
        System.out.println("\n");
        System.out.print("The sentence to reverse is: " + "Good things happen to those who wait");
        System.out.println(" ");
        for (int j = sentence.length - 1; j >= 0; --j) {
            System.out.print(sentence[j]);
        }
    }
}
