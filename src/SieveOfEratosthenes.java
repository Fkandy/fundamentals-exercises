/**
 * Created by a on 2015/10/01.
 */
public class SieveOfEratosthenes {
    public static void main(String args[]) {

        sieveOfEratosthenes(15485863);
    }

    public static void sieveOfEratosthenes(int maxInt) {
        int maxIntRoot = (int) Math.sqrt(maxInt);
        int count = 0;
        boolean[] candidate = new boolean[maxInt + 1];
        for (int m = 2; m < maxIntRoot; m++) {
            if (!candidate[m]) {
                count ++;
                for (int k = m * m; k <= maxInt; k += m)
                    candidate[k] = true;
            }
        }
        for (int m = maxIntRoot; m <= maxInt; m++)
            if (!candidate[m]) {

                count++;
            }
        System.out.println("Number of primes returned of " + maxInt+ " are " + count);
    }
}

