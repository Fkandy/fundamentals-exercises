/**
 * Created by a on 2015/09/21.
 */
public class SumOfArray {
    public static void main(String[] args) {
        int[] numbers = {19, 85, 24, 161, 52, 24, 87};
        int total = 0;
        for (int i : numbers) {
            total += i;
        }
        System.out.println("The sum of all arrays is : " + total);
    }
}

