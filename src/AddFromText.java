import java.io.*;
import java.util.Scanner;

/**
 * Created by a on 2015/09/22.
 */
public class AddFromText {

    public static void main(String[] args) throws IOException {
/*Creating a text file*/
        File file = new File("C:\\Users\\a\\Desktop\\integers.txt");

        file.createNewFile();
        BufferedWriter text = new BufferedWriter(new FileWriter(file));
        text.write(Integer.toString(12));
        text.newLine();
        text.write(Integer.toString(5));
        text.newLine();
        text.write(Integer.toString(20));
        text.newLine();
        text.write(Integer.toString(105));
        text.newLine();
        text.write(Integer.toString(2048));
        text.newLine();
        text.close();

        sumofdigits(file);
    }

    private static void sumofdigits(File file) throws FileNotFoundException {
        FileReader file1 = new FileReader("C:\\Users\\a\\Desktop\\integers.txt");
        int[] digits = new int[5];
        int i = 0;
        int sum = 0;
        Scanner n = new Scanner(file1);
        while (n.hasNext()) {
            digits[i] = n.nextInt();
            i++;
        }
        n.close();
        for (int j : digits) {
            sum += j;

        }
        System.out.println("12 + 5 + 20 + 125 + 2048 = " + sum);
        file.delete();
    }
}
