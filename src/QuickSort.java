import java.util.Arrays;

/**
 * Created by a on 2015/10/01.
 */
public class QuickSort {
    public static void main(String args[]) {
        int[] numbers = {65, 2, 4, 7, 31, 74, 5, 329, 54, 22};

        System.out.println("Before Quick Sort : " + Arrays.toString(numbers));
        quickSort(numbers);
        System.out.println("After Quick Sort : " + Arrays.toString(numbers));

    }

    private static int partition(int[] digits, int i, int j) {
        ;
        int swap;
        int left = i;
        int right = j;
        int pivot = digits[(left + right) / 2];
        while (left <= right) {
            while (digits[left] < pivot) {
                left = left + 1;
            }
            while (digits[right] > pivot) {
                right = right - 1;
            }
            if (left <= right) {
                swap = digits[left];
                digits[left] = digits[right];
                digits[right] = swap;
                left++;
                right--;
            }

        }
        return left;
    }

    private static void recursionSort(int[] digits, int left, int right) {

        int index = partition(digits, left, right);
        if (left < index - 1) {
            recursionSort(digits, left, index - 1);
        }
        if (right > index) {
            recursionSort(digits, index, right);
        }
    }

    private static void quickSort(int[] numbers) {
        recursionSort(numbers, 0, numbers.length - 1);
    }
}
