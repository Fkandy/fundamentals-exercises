import java.util.Random;

/**
 * Created by a on 2015/10/09.
 */
public class AccumulativeSum {
    public static void main(String[] args) {
        double apples = 0.25;
        double pears = 0.5;
        double grapes = 0.2;
        double orange = 0.05;
        double[] weights = {0.25, 0.5, 0.2, 0.05};
        double accumulative_sum = 0.0;
        String[] fruitNames = {"Apples", "Pears", "Grapes", "Orange"};
        for (int j = 0; j < weights.length; j++) {
            accumulative_sum += weights[j];
            System.out.print(fruitNames[j] + " => " + accumulative_sum + "  ");
        }
        System.out.println();
        System.out.print("The selected elements are : ");
        double[] fruit = weightedRandom(apples, pears, grapes, orange);
        for (int i = 0; i < fruit.length; i++) {
            accumulative_sum += fruit[i];
            System.out.print(fruitNames[i] + " ");
        }
    }

    public static double[] weightedRandom(double a, double b, double c, double d) {
        Random random = new Random();
        double[] accumulative_sum = new double[3];
        double sum = 0;
        int min = 0;
        int max = 1;
        int randomNumber = random.nextInt(max) + min;
        if (randomNumber > a) {
            sum += a;
            accumulative_sum[1] = sum;
        }
        if (randomNumber > b) {
            sum += a;
            accumulative_sum[2] = sum;
        }
        if (randomNumber > c) {
            sum += a;
            accumulative_sum[3] = sum;
        }
        if (randomNumber > d) {
            sum += a;
            accumulative_sum[4] = sum;
        }
        return accumulative_sum;
    }
}
