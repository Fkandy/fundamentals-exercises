/**
 * Created by a on 2015/09/22.
 */
public class FindTheMaximum {
    public static void main(String args[]) {
        int[] numbers = {19, 85, 24, 161, 52, 24, 87};

        int largest = numbers[6];

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > largest) {
                largest = numbers[i];
                System.out.println("Largest number in array is :" + largest);
            }


        }

    }
}