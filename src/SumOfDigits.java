import java.util.Scanner;

/**
 * Created by a on 2015/09/21.
 */
public class SumOfDigits {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n;
        System.out.print("Enter a integer: ");
        n = in.nextInt();
        if (n <= 0)
            System.out.println("Integer you've entered is negative.");
        else {
            int total = 0;
            while (n != 0) {
                total += n % 10;
                n /= 10;
            }
            System.out.println("Sum of digits: " + total);
        }
    }
}
