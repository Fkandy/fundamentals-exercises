/**
 * Created by a on 2015/09/21.
 */
public class CallingMethod {
    public static void main(String[] args) {
        valuereturned();  //Calling a method that requires no arguments
        number(5, 5);//second method call. Pass By Value
    }

    //First Method for Obtaining the return value of a method
    public static void valuereturned() {
        {
            System.out.print("Calling a method that requires no arguments");
        }
        System.out.println();
    }

    //second method: Calling a method with a variable number of arguments and create a protected method
    protected static void number(int number1, int number2) {

        int total = number1 * number2;
        System.out.print("Calling a method with a variable number of arguments: " + total);
    }

    private void privatmethod() {
        System.out.println("This is a Private method ");
    }
}