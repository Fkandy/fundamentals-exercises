import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by a on 2015/10/02.
 */
public class MorseCode {
    public static void main(String args[]) {
        Scanner src = new Scanner(System.in);
        System.out.println("Input an english word:");
        String str = src.nextLine();
        String word = str.toUpperCase();
        System.out.println(convertToMorse(word));
        System.out.println("_______________________");
        System.out.println("Input an morse word:");
        String morse = src.nextLine();

        System.out.println(convertToEnglish(morse));


    }

    public static String convertToMorse(String text) {
        HashMap<Character, String> map = new HashMap<Character, String>();
        map.put('A', ".-");
        map.put('B', "-...");
        map.put('C', "-.-.");
        map.put('D', "-..");
        map.put('E', ".");
        map.put('F', "..-.");
        map.put('G', "--.");
        map.put('H', "....");
        map.put('I', "..");
        map.put('J', ".---");
        map.put('K', "-.-");
        map.put('L', ".--..");
        map.put('M', "--");
        map.put('N', "-.");
        map.put('O', "---");
        map.put('P', ".--.");
        map.put('Q', "--.-");
        map.put('R', ".-.");
        map.put('S', "...");
        map.put('T', "-");
        map.put('U', "..-");
        map.put('V', "...-");
        map.put('W', ".--");
        map.put('X', "-..-");
        map.put('Y', "-.--");
        map.put('Z', "--..");
        map.put('1', ".----");
        map.put('2', "..---");
        map.put('3', "...--");
        map.put('4', "....-");
        map.put('5', ".....");
        map.put('6', "-.....");
        map.put('7', "--...");
        map.put('8', "---..");
        map.put('9', "---- .");
        map.put('0', "-----");
        map.put(' ', "  ");

        String vowel = ",";
        String betweenVowels = " ";
        for (int i = 0; i < text.length(); i++) {
            vowel = map.get(text.charAt(i));

            if (vowel.equals(" ")) {
                betweenVowels = betweenVowels + "  ";
            } else {
                betweenVowels = betweenVowels + vowel;

                if (!vowel.equals(" ")) {
                    betweenVowels = betweenVowels + " ";
                }
            }
        }
        return betweenVowels;
    }

    public static String convertToEnglish(String morse) {
        HashMap<String, Character> map = new HashMap<String, Character>();
        map.put(".-", 'A');
        map.put("-...", 'B');
        map.put("-.-.", 'C');
        map.put("-..", 'D');
        map.put(".", 'E');
        map.put("..-.", 'F');
        map.put("--.", 'G');
        map.put("....", 'H');
        map.put("..", 'I');
        map.put(".---", 'J');
        map.put("-.-", 'K');
        map.put(".--..", 'L');
        map.put("--", 'M');
        map.put("-.", 'N');
        map.put("---", 'O');
        map.put(".--.", 'P');
        map.put("--.-", 'Q');
        map.put(".-.", 'R');
        map.put("...", 'S');
        map.put("-", 'T');
        map.put("..-", 'U');
        map.put("...-", 'V');
        map.put(".--", 'W');
        map.put("-..-", 'X');
        map.put("-.--", 'Y');
        map.put("--..", 'Z');
        map.put(".----", '1');
        map.put("..---", '2');
        map.put("...--", '3');
        map.put("....-", '4');
        map.put(".....", '5');
        map.put("-.....", '6');
        map.put("--...", '7');
        map.put("---..", '8');
        map.put("---- .", '9');
        map.put("-----", '0');
        map.put("\n", ' ');

        String[] vowel = morse.split(" ");

        char betweenMorse;
        String strBetweenMorse = " ";
        for (String part : vowel) {
            betweenMorse = map.get((part));
            if (vowel.equals(" ")) {
                strBetweenMorse = betweenMorse + " ";
            } else {
                strBetweenMorse = strBetweenMorse + betweenMorse;
            }
        }

        return strBetweenMorse;


    }
}


