/**
 * Created by a on 2015/09/21.
 */
public class BeerSong {
    public static void main(String[] args) {
        int number = 99;
        String sentence1 = "bottles of beer on the wall";
        String sentence2 = " bottles of beer";
        String sentence3 = "Take one down, pass it around";
        String sentence4 = "bottle of beer on the wall";
        String sentence5 = " bottle of beer";

        for (int i = 99; i >= 0; i--) {
            if (i == 0) {
                System.out.println(i + "  " + sentence4);
            } else {
                System.out.println(i + " " + sentence1 + " \n" + i + sentence2 + " \n" + sentence3);
            }
            if (i == 1) {
                System.out.println(i + " " + sentence4 + " \n" + i + sentence5 + " \n" + sentence3);
            }
        }
    }
}
