/**
 * Created by a on 2015/09/21.
 */
public class TimesTables {
    public static void main(String[] args) {
        int[] number;
        int total;


        for (int i = 1; i <= 12; i++) {

            for (int j = 1; j <= 12; j++) {
                total = i * j;
                System.out.println(i + " x " + j + " = " + total);
            }
            System.out.println("");
        }

    }

}
