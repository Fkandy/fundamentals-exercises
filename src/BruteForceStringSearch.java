import java.util.Scanner;

/**
 * Created by a on 2015/10/07.
 */
public class BruteForceStringSearch {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter a text string: ");
        String fixedString = in.nextLine();
        System.out.print("Enter a pattern string: ");
        String fixedPattern = in.next();

        for (int i = 0; i <= fixedString.length() - fixedPattern.length(); i++) {
            String position = fixedString.substring(i, i + fixedPattern.length());

            if (fixedPattern.equals(position)) {
                System.out.println("Found pattern at position: " + i);
            }
        }
    }
}
