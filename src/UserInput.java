/**
 * Created by a on 2015/09/21.
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

public class UserInput {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter a String:");
        String word = scan.nextLine();

        System.out.println("Enter a the number 75000:");
        int number = Integer.parseInt(scan.next());
        if (number == 75000) {
            System.out.println(word + number);
        } else {
            System.out.println("Your have input a wrong number");
        }


        System.out.println();
    }
}
