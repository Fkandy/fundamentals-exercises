/**
 * Created by a on 2015/10/08.
 */
public class RBGNumbers {
    public static void main(String[] args) {

        byte[] bytes = {(byte) 128, (byte) 192, 79};
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(String.format("%02X ", bytes[i]));
        }
        String[] output = sb.toString().split("\\s+");
        System.out.print("#");
        for (String rbgNumbers : output)
            System.out.print(rbgNumbers);
        }
    }
